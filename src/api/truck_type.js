import request from '@/utils/request-cargo';

export function fetchTruckTypes( params ) {
  return request({
    url: '/truck-type',
    method: 'GET',
    params
  });
}

