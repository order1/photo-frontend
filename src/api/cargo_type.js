import request from '@/utils/request-cargo';

export function fetchCargoTypes( params ) {
  return request({
    url: '/cargo-type',
    method: 'GET',
    params
  });
}
