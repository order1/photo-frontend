import request from '@/utils/request';

export function fetchOptions( params ) {
  return request({
    url: '/option',
    method: 'GET',
    params
  });
}

export function editOption(data) {
  return request({
    url: '/option/' + data.id,
    method: 'PUT',
    data
  });
}
