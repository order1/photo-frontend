import request from '@/utils/request';
import { CancelToken } from 'axios';

// const CancelToken = request.CancelToken;
let tags_loading_source = void 0;

export function fetchTags(params) {
  if (tags_loading_source) {
    tags_loading_source.cancel('OPERATION_CANCELED');
  }

  tags_loading_source = CancelToken.source();

  return new Promise((resolve, reject) => {
    request({
      url: '/tag',
      method: 'GET',
      params,
      cancelToken: tags_loading_source.token
    }).then(data => {
      resolve(data)
      tags_loading_source = void 0;
    }).catch(error => {
      reject(error);
    }).finally(() => {
      //
    });
  });
}

export function createTag(data) {
  return request({
    url: '/tag',
    method: 'POST',
    data
  });
}

export function editTag(data) {
  return request({
    url: '/tag/' + data.id,
    method: 'PUT',
    data
  });
}

export function deleteTag(data) {
  return request({
    url: '/tag/' + data.id,
    method: 'DELETE'
  });
}
