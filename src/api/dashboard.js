import request from '@/utils/request';

export function fetchDashboard( params ) {
  return request({
    url: '/dashboard',
    method: 'GET',
    params
  });
}

