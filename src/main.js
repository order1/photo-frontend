import Vue from 'vue';
import App from './App.vue';
import vuetify from './plugins/vuetify';
import router from './router';
import Vuex from 'vuex';
import store from './store';
import Vuelidate from 'vuelidate';
// import VueMask from 'v-mask'
import Cloudinary, { CldImage, CldTransformation } from "cloudinary-vue";

Vue.config.productionTip = false;

Vue.use(Vuex, vuetify, Vuelidate);
// Vue.use(VueMask);

Vue.use(Cloudinary, {
  configuration: { cloudName: "demo" },
  components: {
    CldImage,
    CldTransformation
  }
});

new Vue({
  vuetify,
  router,
  store,
  render: h => h(App)
}).$mount('#app')
