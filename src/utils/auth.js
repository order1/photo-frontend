import * as ls from 'local-storage';

const TokenKey = 'vue_token';
const UserKey = 'vue_user';

export function getToken() {
  return ls.get(TokenKey);
}

export function setToken(token) {
  return ls.set(TokenKey, token);
}

export function removeToken() {
  return ls.remove(TokenKey);
}

// --
export function getUser() {
  return ls.get(UserKey);
}

export function setUser(user) {
  return ls.set(UserKey, user);
}

export function logOutUser() {
  return ls.remove(UserKey);
}
