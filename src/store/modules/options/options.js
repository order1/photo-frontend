import Vue from 'vue'
import {
  fetchOptions,
  editOption
} from '@/api/option';

import {
  FETCH_OPTIONS,
  EDIT_OPTION,
} from '@/store/mutation-types';

const state = () => {
  return {
    // option: void 0,
    options: [],
  };
};

const getters = {
  options: state => {
    return state.options
  },
};

const mutations = {
  [FETCH_OPTIONS](state, payload) {
    state.options = payload;
  },
  [EDIT_OPTION](state, payload) {
    const index = state.options.findIndex(item => item.id === payload.id);
    Vue.set(state.options, index, payload);
  },
};

const actions = {
  async fetchOptions({
    commit
  }, params) {
    try {
      const data = await fetchOptions(params);

      commit(FETCH_OPTIONS, data);
    } catch (error) {
      console.error(error);
      return Promise.reject(error)
    }
  },

  async editOption({
    commit
  }, payload) {
    // commit('clearError')
    // commit('setLoading', true);

    try {
      let {
        data
      } = await editOption(payload);
      commit(EDIT_OPTION, data);

      // commit('setLoading', false);
      // commit('createNewShift', data)
    } catch (error) {
      console.error(error);
      return Promise.reject(error);
    }
  },
};

export default {
  namespaced: true,
  state,
  getters,
  mutations,
  actions
};
