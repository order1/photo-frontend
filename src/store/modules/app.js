import { SET_LOADING, SET_ERROR, CLEAR_ERROR } from '@/store/mutation-types';

const state = {
  loading: false,
  error: null,
  isLogged: false,
  imagesBaseUrl: process.env.VUE_APP_BASE_IMAGES,
  cloudName: process.env.VUE_APP_CLOUDINARY_NAME,
};

const getters = {
  loading: state => {
    return state.loading
  },
  error: state => {
    return state.error
  },
  imagesBaseUrl: state => {
    return state.imagesBaseUrl
  },
  cloudName: state => {
    return state.cloudName
  }
};

const mutations = {
  [SET_LOADING](state, payload) {
    state.loading = payload
  },
  [SET_ERROR](state, payload) {
    state.error = payload
  },
  [CLEAR_ERROR](state) {
    state.error = null
  },
};

const actions = {
  setLoading({ commit }, payload) {
    commit('SET_LOADING', payload);
  },
  setError({ commit }, payload) {
    commit('SET_ERROR', payload);
  },
  clearError({ commit }) {
    commit('CLEAR_ERROR');
  },
};

export default {
  namespaced: true,
  state,
  getters,
  mutations,
  actions
};
