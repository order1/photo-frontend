import Vue from 'vue'
import {
  fetchTags,
  createTag,
  editTag,
  deleteTag
} from '@/api/tag';

import {
  FETCH_TAGS,
  CREATE_TAG,
  EDIT_TAG,
  DELETE_TAG,
  SET_TAG_PAGE,
  SET_TAG_TOTAL_PAGES,
  SET_TAG_SEARCH,
} from '@/store/mutation-types';

const state = () => {
  return {
    // tag: void 0,
    tags: [],
    page: 1,
    total_pages: 1,
    per_page: 15,
    // filters:
    search: void 0,
  };
};

const getters = {
  tags: state => {
    return state.tags
  },
  page: state => {
    return state.page
  },
  total_pages: state => {
    return state.total_pages
  },
  per_page: state => {
    return state.per_page
  },
  search: state => {
    return state.search
  },
};

const mutations = {
  [FETCH_TAGS](state, payload) {
    state.tags = payload;
  },
  [CREATE_TAG](state, payload) {
    state.tags.push(payload);
  },
  [EDIT_TAG](state, payload) {
    const index = state.tags.findIndex(item => item.id === payload.id);
    Vue.set(state.tags, index, payload);
  },
  [DELETE_TAG](state, payload) {
    state.tags.map((tag, index) => {
      if (tag.id == payload.id) {
        state.tags.splice(index, 1);
      }
    })
  },
  [SET_TAG_PAGE](state, payload) {
    state.page = payload;
  },
  [SET_TAG_TOTAL_PAGES](state, payload) {
    state.total_pages = payload;
  },
  [SET_TAG_SEARCH](state, payload) {
    state.search = payload;
  },
};

const actions = {
  async fetchTags({
    commit, state
  }, params = {}) {
    try {
      // filters
      params.per_page = state.per_page;
      params.page = state.page;
      if (!params.search) {
        params.search = state.search ? state.search : void 0;
      }

      const response = await fetchTags(params);

      if (!response.success || !response.data) {
        console.log(response);
        if (response.message) {
          return Promise.reject(response.message);
        }

        return Promise.reject('Error. Can\'t load tags.')
      }

      commit(SET_TAG_TOTAL_PAGES, Math.ceil(response.total / state.per_page));
      commit(FETCH_TAGS, response.data);
    } catch (error) {
      console.error(error);
      return Promise.reject(error)
    }
  },

  async createNewTag({
    commit
  }, payload) {
    // commit('clearError')
    // commit('setLoading', true);

    try {
      let {
        data
      } = await createTag(payload);
      commit(CREATE_TAG, data);

      // commit('setLoading', false);
      // commit('createNewShift', data)
      return data;
    } catch (error) {
      console.error(error);
      return Promise.reject(error);
    }
  },

  async editTag({
    commit
  }, payload) {
    // commit('clearError')
    // commit('setLoading', true);

    try {
      let {
        data
      } = await editTag(payload);
      commit(EDIT_TAG, data);

      // commit('setLoading', false);
      // commit('createNewShift', data)
    } catch (error) {
      console.error(error);
      return Promise.reject(error);
    }
  },

  async deleteTag({
    commit
  }, payload) {
    // commit('clearError')
    // commit('setLoading', true);

    try {
      let {
        data
      } = await deleteTag(payload);
      commit(DELETE_TAG, data);

      // commit('setLoading', false);
      // commit('createNewShift', data)
    } catch (error) {
      console.error(error);
      return Promise.reject(error);
    }
  },
  //
  // --
  setPage({
    commit
  }, page) {
    commit(SET_TAG_PAGE, page);
  },
  setSearch({
    commit
  }, search) {
    commit(SET_TAG_SEARCH, search);
  },
};

export default {
  namespaced: true,
  state,
  getters,
  mutations,
  actions
};
