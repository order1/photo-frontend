import {
  fetchTruckTypes,
} from '@/api/truck_type';

import {
  FETCH_TRUCK_TYPES,
} from '@/store/mutation-types';

const state = () => {
  return {
    // truck_type: void 0,
    truck_types: [],
  };
};

const getters = {
  truck_types: state => {
    return state.truck_types
  },
};

const mutations = {
  [FETCH_TRUCK_TYPES](state, payload) {
    state.truck_types = payload;
  },
};

const actions = {
  async fetchTruckTypes({
    commit
  }, params) {
    try {
      const response = await fetchTruckTypes(params);

      if (!response.success || !response.data) {
        console.log(response);
        if (response.message) {
          return Promise.reject(response.message);
        }

        return Promise.reject('Error. Can\'t load truck_types.')
      }

      commit(FETCH_TRUCK_TYPES, response.data);
    } catch (error) {
      console.error(error);
      return Promise.reject(error)
    }
  },
};

export default {
  namespaced: true,
  state,
  getters,
  mutations,
  actions
};
