import {
  fetchDashboard
} from '@/api/dashboard';

import {
  FETCH_DASHBOARD,
} from '@/store/mutation-types';

const state = () => {
  return {
    // option: void 0,
    dashboard_data: void 0,
  };
};

const getters = {
  dashboard_data: state => {
    return state.dashboard_data
  },
};

const mutations = {
  [FETCH_DASHBOARD](state, payload) {
    state.dashboard_data = payload;
  },
};

const actions = {
  async fetchDashboard({
    commit
  }, params) {
    try {
      const response = await fetchDashboard(params);

      if (!response.success || !response.data) {
        if (response.message) {
          return Promise.reject(response.message);
        }

        return Promise.reject('Error. Can\'t load totals.')
      }

      commit(FETCH_DASHBOARD, response.data);
    } catch (error) {
      console.error(error);
      return Promise.reject(error)
    }
  },

};

export default {
  namespaced: true,
  state,
  getters,
  mutations,
  actions
};
