const getters = {
  _token: state => state.tokenHandler.token,
  _isAuth: state => state.tokenHandler.isAuth,
  // user: state => state.user.user,
};
export default getters;
