import Vue from "vue"
import Vuex from 'vuex'
import getters from '@/store/getters';
import app from '@/store/modules/app';
import user from '@/store/modules/user/user';
import cargo_type from '@/store/modules/images/cargo_type';
import tag from '@/store/modules/images/tag';
import truck_type from '@/store/modules/images/truck_type';
import image from '@/store/modules/images/image';
import options from '@/store/modules/options/options';
import dashboard from '@/store/modules/dashboard/dashboard';
import tokenHandler from '@/store/modules/tokenHandler';

Vue.use(Vuex)

export default new Vuex.Store({
    modules: {
        app,
        user,
        cargo_type,
        tag,
        truck_type,
        image,
        options,
        dashboard,
        tokenHandler,
    },
    getters
})
