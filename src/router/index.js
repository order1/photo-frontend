import Vue from 'vue'
import VueRouter from 'vue-router'
import LogIn from '@/components/Auth/LogIn.vue'
import Profile from '@/components/Auth/Profile.vue'
import Dashboard from '@/views/Dashboard/Dashboard.vue'
import Users from '@/views/Users/Users.vue'
import Images from '@/views/Images/Images.vue'
import ImagesModerate from '@/views/Images/ImagesModerate.vue'
import Tags from '@/views/Tags/Tags.vue'
import Settings from '@/views/Settings/Settings.vue'

Vue.use(VueRouter)

const routes = [
  {
    path: '/login', // administrator
    props: true,
    name: 'LogIn',
    component: LogIn,
  },
  {
    path: '/profile',
    props: true,
    name: 'Profile',
    component: Profile,
},
  {
    path: '/dashboard',
    name: 'Dashboard',
    component: Dashboard
  },
  {
    path: '/users',
    name: 'Users',
    component: Users
  },
  {
    path: '/images',
    name: 'Images',
    component: Images
  },
  {
    path: '/images-moderate',
    name: 'ImagesModerate',
    component: ImagesModerate
  },
  {
    path: '/tags',
    name: 'Tags',
    component: Tags
  },
  {
    path: '/settings',
    name: 'Settings',
    component: Settings
  },
  // {
  //   path: '/about',
  //   name: 'About',
  //   // route level code-splitting
  //   // this generates a separate chunk (about.[hash].js) for this route
  //   // which is lazy-loaded when the route is visited.
  //   component: () => import(/* webpackChunkName: "about" */ '../views/About.vue')
  // }
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router
